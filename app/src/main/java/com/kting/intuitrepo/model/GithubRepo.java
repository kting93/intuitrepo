package com.kting.intuitrepo.model;



import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.Date;

public class GithubRepo {
    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("owner")
    private GithubUser owner;
    @SerializedName("description")
    private String description;
    @SerializedName("stargazers_count")
    private Integer stars;
    @SerializedName("forks_count")
    private Integer forks;
    @SerializedName("has_issues")
    private Boolean hasIssues;
    @SerializedName("open_issues")
    private Integer openIssues;
    @SerializedName("watchers_count")
    private Integer watchers;
    @SerializedName("created_at")
    private Date created;
    @SerializedName("updated_at")
    private Date updated;

    public GithubRepo(Integer id, String name, String fullName, Integer stars, Integer forks, Boolean hasIssues, Integer openIssues, Integer watchers) {
        this.id = id;
        this.name = name;
        this.fullName = fullName;
        this.stars = stars;
        this.forks = forks;
        this.hasIssues = hasIssues;
        this.openIssues = openIssues;
        this.watchers = watchers;
//        this.created = created;
//        this.updated = updated;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public GithubUser getOwner() {
        return owner;
    }

    public Integer getStars() {
        return stars;
    }

    public Integer getForks() {
        return forks;
    }

    public Boolean getHasIssues() {
        return hasIssues;
    }

    public Integer getOpenIssues() {
        return openIssues;
    }

    public Integer getWatchers() {
        return watchers;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

}
