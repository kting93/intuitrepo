package com.kting.intuitrepo.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class GithubIssue {

    @SerializedName("url")
    private String url;
    @SerializedName("id")
    private Integer id;
    @SerializedName("number")
    private Integer number;
    @SerializedName("title")
    private String title;
    @SerializedName("user")
    private GithubUser user;
    @SerializedName("state")
    private String state;
    @SerializedName("locked")
    private Boolean locked;
    @SerializedName("comments")
    private Integer commentCount;
    @SerializedName("body")
    private String body;
    @SerializedName("created_at")
    private Date created;
    @SerializedName("updated_at")
    private Date updated;

    public String getUrl() {
        return url;
    }

    public Integer getId() {
        return id;
    }

    public Integer getNumber() {
        return number;
    }

    public String getTitle() {
        return title;
    }

    public GithubUser getUser() {
        return user;
    }

    public String getBody() {
        return body;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }
}
