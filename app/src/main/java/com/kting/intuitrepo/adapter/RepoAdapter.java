package com.kting.intuitrepo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.chip.Chip;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextClock;
import android.widget.TextView;

import com.kting.intuitrepo.R;
import com.kting.intuitrepo.activity.RepoDetailActivity;
import com.kting.intuitrepo.model.GithubRepo;

import java.util.List;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.RepoViewHolder> {

    public static final String REPO = "name";

    private List<GithubRepo> repos;
    private Context context;

    public RepoAdapter(Context context, List<GithubRepo> repos) {
        this.context = context;
        this.repos = repos;
    }

    class RepoViewHolder extends RecyclerView.ViewHolder {

        public final View repoListItem;

        TextView titleView;
        TextView descView;
        Chip starCount;
        Chip forkCount;
        Chip issueCount;

        public RepoViewHolder(@NonNull View itemView) {
            super(itemView);
            repoListItem = itemView;

            titleView = repoListItem.findViewById(R.id.title);
            descView = repoListItem.findViewById(R.id.repo_row_desc);
            starCount = repoListItem.findViewById(R.id.starCount);
            forkCount = repoListItem.findViewById(R.id.forkCount);
            issueCount = repoListItem.findViewById(R.id.issueCount);
        }
    }

    @Override
    public RepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_repo, parent, false);
        return new RepoAdapter.RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RepoAdapter.RepoViewHolder repoViewHolder, int position) {

        final GithubRepo currentRepo = repos.get(position);

        repoViewHolder.titleView.setText(currentRepo.getName());
        repoViewHolder.descView.setText(currentRepo.getDescription());
        repoViewHolder.starCount.setChipText(currentRepo.getStars().toString());
        repoViewHolder.forkCount.setChipText(currentRepo.getForks().toString());
        repoViewHolder.issueCount.setChipText(currentRepo.getOpenIssues().toString());

        repoViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent repoDetailIntent = new Intent(view.getContext(), RepoDetailActivity.class);
                repoDetailIntent.putExtra(REPO, currentRepo.getName());
                view.getContext().startActivity(repoDetailIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (repos == null) {
            return 0;
        }
        return repos.size();
    }
}
