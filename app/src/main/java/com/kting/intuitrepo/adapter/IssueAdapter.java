package com.kting.intuitrepo.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kting.intuitrepo.R;
import com.kting.intuitrepo.activity.IssueDetailActivity;
import com.kting.intuitrepo.model.GithubIssue;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

public class IssueAdapter extends RecyclerView.Adapter<IssueAdapter.IssueViewHolder> {

    public static final String ISSUE = "issue";

    private Context context;
    private List<GithubIssue> issues;
    private String repoName;

    public IssueAdapter(Context context, List<GithubIssue> issues, String repoName) {
        this.context = context;
        this.issues = issues;
        this.repoName = repoName;
    }

    @NonNull
    @Override
    public IssueViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_issue, parent, false);
        return new IssueAdapter.IssueViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IssueViewHolder issueViewHolder, int position) {

        final GithubIssue currentIssue = issues.get(position);

        issueViewHolder.issueTitle.setText(currentIssue.getTitle());
        issueViewHolder.username.setText(currentIssue.getUser().getLogin());

        Picasso.with(context).load(currentIssue.getUser().getAvatarUrl())
                .placeholder(R.drawable.ic_face_black_24dp)
                .into(issueViewHolder.userPortrait);

        issueViewHolder.issueCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent issueDetailIntent = new Intent(v.getContext(), IssueDetailActivity.class);
                issueDetailIntent.putExtra(RepoAdapter.REPO, repoName);
                issueDetailIntent.putExtra(ISSUE, currentIssue.getNumber());
                ((Activity) v.getContext()).startActivityForResult(issueDetailIntent, 1);
            }
        });

    }

    @Override
    public int getItemCount() {
        return issues.size();
    }

    class IssueViewHolder extends RecyclerView.ViewHolder {
        public final View issueListItem;

        CardView issueCard;
        ImageView userPortrait;
        TextView issueTitle;
        TextView username;

        public IssueViewHolder(@NonNull View itemView) {

            super(itemView);
            issueListItem = itemView;

            issueCard = issueListItem.findViewById(R.id.issue_list_card);
            issueTitle = issueListItem.findViewById(R.id.issue_list_title);
            userPortrait = issueListItem.findViewById(R.id.issue_user_portrait);
            username = issueListItem.findViewById(R.id.issue_list_user_name);
        }
    }
}
