package com.kting.intuitrepo.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/*
    Retrofit instance that interfaces with GitHub's API
 */
public class GithubRetrofitInstance {

    private static Retrofit retrofit;

    public static Retrofit getGithubServiceInstance() {
        if (retrofit == null) {

            Gson gson = new GsonBuilder().serializeNulls().setDateFormat(DateFormat.LONG).create();
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.github.com")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}
