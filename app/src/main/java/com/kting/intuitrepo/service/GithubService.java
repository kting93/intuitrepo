package com.kting.intuitrepo.service;

import com.kting.intuitrepo.model.GithubIssue;
import com.kting.intuitrepo.model.GithubRepo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GithubService {
    @GET("/users/{user}/repos")
    Call<List<GithubRepo>> getReposByUser(@Path("user") String user);

    @GET("/repos/{user}/{repo}")
    Call<GithubRepo> getRepo(@Path("user") String user, @Path("repo") String repo);

    @GET("/repos/{user}/{repo}/issues")
    Call<List<GithubIssue>> getEvents(@Path("user") String user, @Path("repo") String repo);

    @GET("/repos/{user}/{repo}/issues/{issue}")
    Call<GithubIssue> getIssue(@Path("user") String user, @Path("repo") String repo, @Path("issue") Integer issue);
}
