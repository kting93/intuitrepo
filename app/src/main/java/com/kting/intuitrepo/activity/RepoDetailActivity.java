package com.kting.intuitrepo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kting.intuitrepo.R;
import com.kting.intuitrepo.adapter.RepoAdapter;
import com.kting.intuitrepo.model.GithubRepo;
import com.kting.intuitrepo.service.GithubRetrofitInstance;
import com.kting.intuitrepo.service.GithubService;
import com.kting.intuitrepo.utility.Resource;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoDetailActivity extends BaseActivity {

    private String repoName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadView();
    }

    private void loadView() {
        setContentView(R.layout.activity_repo_detail);

        Bundle extras = getIntent().getExtras();

        repoName = extras.getString(RepoAdapter.REPO);

        if (repoName != null) {

            super.setToolbarTitle(repoName);

            GithubService service = GithubRetrofitInstance.getGithubServiceInstance().create(GithubService.class);

            Call<GithubRepo> call = service.getRepo(Resource.user, repoName);
            final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading " + repoName);
            call.enqueue(new Callback<GithubRepo>() {
                @Override
                public void onResponse(Call<GithubRepo> call, Response<GithubRepo> response) {
                    GithubRepo repo = response.body();
                    dialog.dismiss();
                    populateRepoDetails(repo);
                }

                @Override
                public void onFailure(Call<GithubRepo> call, Throwable t) {
                    Log.e("REPO DETAIL", "Failed to get Repo details...", t);
                    setContentView(R.layout.network_failure_layout);
                    View v = findViewById(R.id.network_error_layout);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loadView();
                        }
                    });
                    Toast.makeText(RepoDetailActivity.this, "Failed to retrieve repository...", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } else {
            Log.e("REPO DETAIL", "Repo name was null");
            setContentView(R.layout.network_failure_layout);
            View v = findViewById(R.id.network_error_layout);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadView();
                }
            });
            Toast.makeText(RepoDetailActivity.this, "Failed to retrieve repository...", Toast.LENGTH_SHORT).show();
        }
    }

    private void populateRepoDetails(GithubRepo repo) {
        TextView title = findViewById(R.id.repo_detail_title);
        TextView description = findViewById(R.id.repo_detail_description);
        TextView stars = findViewById(R.id.repo_detail_stars);
        TextView forks = findViewById(R.id.repo_detail_forks);
        TextView watchers = findViewById(R.id.repo_detail_watchers);
        TextView issues = findViewById(R.id.repo_detail_issues);
        CardView issuesCard = findViewById(R.id.repo_detail_issues_card);

        stars.setText(String.format(Locale.getDefault(), "%d", repo.getStars()));
        forks.setText(String.format(Locale.getDefault(), "%d", repo.getForks()));
        watchers.setText(String.format(Locale.getDefault(), "%d", repo.getWatchers()));

        ImageView ownerPortrait = findViewById(R.id.repo_detail_owner_portrait);
        TextView ownerName = findViewById(R.id.repo_detail_owner_name);
//
        title.setText(repo.getName());
        description.setText(repo.getDescription());

        if (!repo.getOpenIssues().equals(0)) {

            issues.setText(String.format(Locale.getDefault(), "%d", repo.getOpenIssues()));
            issuesCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent issueListIntent = new Intent(view.getContext(), IssueListActivity.class);
                    issueListIntent.putExtras(getIntent().getExtras());
                    RepoDetailActivity.this.startActivityForResult(issueListIntent, 1);

                }
            });
        } else {

            ImageView issuesIcon = findViewById(R.id.repo_detail_issues_icon);
            issuesIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_black_24dp));
            issuesCard.setCardBackgroundColor(getResources().getColor(R.color.green));
            issues.setVisibility(View.GONE);

        }

        Picasso.with(this).load(repo.getOwner().getAvatarUrl())
                .placeholder(R.drawable.ic_face_black_24dp)
                .into(ownerPortrait);
        ownerName.setText(repo.getOwner().getLogin());

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                repoName = data.getStringExtra(RepoAdapter.REPO);
            }
        }
    }
}
