package com.kting.intuitrepo.activity;

import android.app.ProgressDialog;;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;


/*
    Base activity class that holds fucntions used by all other activities.
 */
public class BaseActivity extends AppCompatActivity {

    private ActionBar toolbar;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar = this.getSupportActionBar();
        if (toolbar != null) {
            toolbar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }

    /*
        Function that handles back button tapped.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass bundle back to previous activity
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();

            if (this.getIntent() != null) {
                intent.putExtras(this.getIntent().getExtras());
                setResult(RESULT_OK, intent);
            }
            this.finish();
        }
        return true;
    }

    public int pixelToDp(int px) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (px * scale + 0.5f);
    }


}
