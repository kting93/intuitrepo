package com.kting.intuitrepo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.kting.intuitrepo.R;
import com.kting.intuitrepo.adapter.IssueAdapter;
import com.kting.intuitrepo.adapter.RepoAdapter;
import com.kting.intuitrepo.model.GithubIssue;
import com.kting.intuitrepo.service.GithubRetrofitInstance;
import com.kting.intuitrepo.service.GithubService;
import com.kting.intuitrepo.utility.Resource;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueListActivity extends BaseActivity {

    private String repoName;

    SwipeRefreshLayout refreshLayout;

    RecyclerView recyclerView;
    IssueAdapter adapter;

    private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadView();

    }

    private void loadView() {
        setContentView(R.layout.activity_issue_list);

        refreshLayout = findViewById(R.id.issue_swipe_to_refresh);
        recyclerView = findViewById(R.id.issue_recycler_view);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadIssueRecyclerView();
            }
        });

        loadIssueRecyclerView();

    }
    
    private void loadIssueRecyclerView() {

        Bundle extras = getIntent().getExtras();
        repoName = extras.getString(RepoAdapter.REPO);

        super.setToolbarTitle(repoName + " Issues");

        GithubService service = GithubRetrofitInstance.getGithubServiceInstance()
                .create(GithubService.class);

        Call<List<GithubIssue>> call = service.getEvents(Resource.user, repoName);

        dialog = ProgressDialog.show(this, "", "Loading Issues List");

        call.enqueue(new Callback<List<GithubIssue>>() {
            @Override
            public void onResponse(Call<List<GithubIssue>> call, Response<List<GithubIssue>> response) {
                if (response.body() == null) {
                    setContentView(R.layout.network_failure_layout);

                    View v = findViewById(R.id.network_error_layout);

                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadView();
                        }
                    });

                    Toast.makeText(IssueListActivity.this, "Failed to retrieve repositories...", Toast.LENGTH_SHORT).show();
                } else {
                    adapter = new IssueAdapter(getBaseContext(), response.body(), repoName);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(IssueListActivity.this);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapter);
                    dialog.dismiss();
                    refreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<List<GithubIssue>> call, Throwable t) {
                setContentView(R.layout.network_failure_layout);

                View v = findViewById(R.id.network_error_layout);

                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadView();
                    }
                });

                Log.e("ISSUE ACTIVITY", "failed to retrieve issues...", t);
                Toast.makeText(IssueListActivity.this, "Failed to retrieve issues...", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                refreshLayout.setRefreshing(false);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                repoName = data.getStringExtra(RepoAdapter.REPO);
            }
        }
    }

}
