package com.kting.intuitrepo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kting.intuitrepo.R;
import com.kting.intuitrepo.adapter.IssueAdapter;
import com.kting.intuitrepo.adapter.RepoAdapter;
import com.kting.intuitrepo.model.GithubIssue;
import com.kting.intuitrepo.model.GithubUser;
import com.kting.intuitrepo.service.GithubRetrofitInstance;
import com.kting.intuitrepo.service.GithubService;
import com.kting.intuitrepo.utility.Resource;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueDetailActivity extends BaseActivity {

    private Integer issueNum;
    private String repoName;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadView();
    }

    private void loadView() {
        setContentView(R.layout.activity_issue_detail);

        Bundle extras = getIntent().getExtras();
        issueNum = extras.getInt(IssueAdapter.ISSUE);
        repoName = extras.getString(RepoAdapter.REPO);

        if (issueNum != null) {
            GithubService service = GithubRetrofitInstance.getGithubServiceInstance()
                    .create(GithubService.class);
            Call<GithubIssue> call = service.getIssue(Resource.user, repoName, issueNum);

            dialog = ProgressDialog.show(IssueDetailActivity.this, "", "Loading Issue Details");
            call.enqueue(new Callback<GithubIssue>() {
                @Override
                public void onResponse(Call<GithubIssue> call, Response<GithubIssue> response) {
                    populateIssueDetail(response.body());
                }

                @Override
                public void onFailure(Call<GithubIssue> call, Throwable t) {
                    setContentView(R.layout.network_failure_layout);
                    View v = findViewById(R.id.network_error_layout);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loadView();
                        }
                    });
                    Log.e("ISSUE ACTIVITY", "failed to retrieve issues...", t);
                    Toast.makeText(IssueDetailActivity.this, "Failed to retrieve issues...", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                repoName = data.getStringExtra(RepoAdapter.REPO);
                issueNum = data.getIntExtra(IssueAdapter.ISSUE, 0);
            }
        }
    }

    public void populateIssueDetail(GithubIssue issue) {
        GithubUser user = issue.getUser();

        TextView num = findViewById(R.id.issue_detail_num);
        TextView title = findViewById(R.id.issue_detail_title);
        TextView body = findViewById(R.id.issue_detail_body);

        ImageView userPortrait = findViewById(R.id.issue_detail_owner_portrait);
        TextView userName = findViewById(R.id.issue_detail_owner_name);

        Picasso.with(this).load(user.getAvatarUrl())
                .placeholder(R.drawable.ic_face_black_24dp)
                .into(userPortrait);

        userName.setText(user.getLogin());

        num.setText(String.format(Locale.getDefault(), "Issue #%d", issue.getNumber()));
        title.setText(issue.getTitle());
        body.setText(issue.getBody());
        dialog.dismiss();
    }
}
