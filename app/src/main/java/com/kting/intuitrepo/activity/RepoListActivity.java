package com.kting.intuitrepo.activity;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.kting.intuitrepo.R;
import com.kting.intuitrepo.adapter.RepoAdapter;
import com.kting.intuitrepo.model.GithubRepo;
import com.kting.intuitrepo.service.GithubRetrofitInstance;
import com.kting.intuitrepo.service.GithubService;
import com.kting.intuitrepo.utility.Resource;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoListActivity extends AppCompatActivity {

    private SwipeRefreshLayout refreshLayout;

    private RecyclerView recyclerView;
    private RepoAdapter adapter;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadView();

    }

    private void loadView() {
        setContentView(R.layout.activity_repo);

        refreshLayout = findViewById(R.id.repo_swipe_to_refresh);
        recyclerView = findViewById(R.id.repo_recycler_view);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadRepoRecyclerView();
            }
        });

        loadRepoRecyclerView();
    }

    private void loadRepoRecyclerView() {
        GithubService service = GithubRetrofitInstance.getGithubServiceInstance()
                .create(GithubService.class);

        Call<List<GithubRepo>> call = service.getReposByUser(Resource.user);
        dialog = ProgressDialog.show(this, "", "Loading Repo List");
        call.enqueue(new Callback<List<GithubRepo>>() {
            @Override
            public void onResponse(Call<List<GithubRepo>> call, Response<List<GithubRepo>> response) {
                dialog.dismiss();
                if (response.body() == null) {
                    setContentView(R.layout.network_failure_layout);
                    Toast.makeText(RepoListActivity.this, "Failed to retrieve repositories...", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } else {
                    adapter = new RepoAdapter(getBaseContext(), response.body());
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(RepoListActivity.this);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapter);
                    dialog.dismiss();
                    refreshLayout.setRefreshing(false);
                }

            }

            @Override
            public void onFailure(Call<List<GithubRepo>> call, Throwable t) {
                setContentView(R.layout.network_failure_layout);
                View v = findViewById(R.id.network_error_layout);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadView();
                    }
                });
                Log.e("REPO ACTIVITY", "failed to retrieve repos...", t);
                Toast.makeText(RepoListActivity.this, "Failed to retrieve repositories...", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                refreshLayout.setRefreshing(false);
            }
        });
    }

}
