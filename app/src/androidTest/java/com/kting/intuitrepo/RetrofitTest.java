package com.kting.intuitrepo;

import android.support.test.runner.AndroidJUnit4;
import android.support.v4.widget.TextViewCompat;

import com.kting.intuitrepo.model.GithubIssue;
import com.kting.intuitrepo.model.GithubRepo;
import com.kting.intuitrepo.service.GithubRetrofitInstance;
import com.kting.intuitrepo.service.GithubService;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
public class RetrofitTest {
    @Test
    public void createGithubService() {
        GithubService service = GithubRetrofitInstance.getGithubServiceInstance()
                .create(GithubService.class);

        assertNotNull(service);
    }

    @Test
    public void getSingleRepo() {
        GithubService service = GithubRetrofitInstance.getGithubServiceInstance()
                .create(GithubService.class);

        Call<GithubRepo> call = service.getRepo("intuit", "ami-query");
        call.enqueue(new Callback<GithubRepo>() {
            @Override
            public void onResponse(Call<GithubRepo> call, Response<GithubRepo> response) {
                assertNotNull(response.body());
                assertNotNull(response.body().getId());
            }

            @Override
            public void onFailure(Call<GithubRepo> call, Throwable t) {
                fail("request failed");
            }
        });
    }

    @Test
    public void getRepoList() {
        GithubService service = GithubRetrofitInstance.getGithubServiceInstance()
                .create(GithubService.class);

        Call<List<GithubRepo>> call = service.getReposByUser("intuit");
        call.enqueue(new Callback<List<GithubRepo>>() {
            @Override
            public void onResponse(Call<List<GithubRepo>> call, Response<List<GithubRepo>> response) {
                assertNotNull(response.body());
                assertNotEquals(0, response.body().size());
            }

            @Override
            public void onFailure(Call<List<GithubRepo>> call, Throwable t) {
                fail("request failed");
            }
        });
    }

    @Test
    public void getIssue() {
        GithubService service = GithubRetrofitInstance.getGithubServiceInstance()
                .create(GithubService.class);

        Call<GithubIssue> call = service.getIssue("intuit", "ami-query", 9);
        call.enqueue(new Callback<GithubIssue>() {
            @Override
            public void onResponse(Call<GithubIssue> call, Response<GithubIssue> response) {
                assertNotNull(response.body());
            }

            @Override
            public void onFailure(Call<GithubIssue> call, Throwable t) {
                fail("request failed");
            }
        });
    }
}
